'use strict';

// класс Vector позволяет контролировать расположение объектов в двумерном пространстве и управлять их размером и перемещением

class Vector { 
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  plus(vector) {
    if (!(vector instanceof Vector)) {
      throw new Error(`В метод plus передан не объект класса Vector`);
    }
    return new Vector((this.x + vector.x), this.y + vector.y);     
  }

  times(factor) {
    return new Vector((this.x * factor), this.y * factor);
  }
}

// класс Actor позволяет контролировать все движущиеся объекты на игровом поле и контролировать их пересечение

class Actor { 
  constructor(position = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
    if (!(position instanceof Vector) || !(size instanceof Vector) || !(speed instanceof Vector)) {
      throw new Error(`В конструктор передан не объект класса Vector`);
    }
    this.pos = position;
    this.size = size;
    this.speed = speed;
  }

  act() {

  }

  get type() {
    return 'actor'
  }

  get left() { // левая граница объекта
    return this.pos.x;
  }

  get right() { // правая граница объекта
    return this.pos.x + this.size.x;
  }

  get top() { // верхняя граница объекта
    return this.pos.y;
  }

  get bottom() { // нижняя граница объекта
    return this.pos.y + this.size.y;
  }

  isIntersect(otherActor) { // проверяем, пересекается ли текущий объект с переданным объектом, и если да, возвращаем true, иначе – false.
    if (!(otherActor instanceof Actor)) {
      throw new Error(`В качестве аргумента передан не объект класса Actor`);
    } 
    if (this === otherActor) {
      return false;
    }
    return this.right > otherActor.left && 
           this.left < otherActor.right && 
           this.top < otherActor.bottom && 
           this.bottom > otherActor.top;
  }
}

// класс Level реализует схему игрового поля конкретного уровня,
// контролирует все движущиеся объекты на нём и реализует логику игры

class Level {
  constructor(grid = [], actorsArray = []) { //grid[] - сетка игрового поля, actorsArray - список движущихся объектов игрового поля
    this.grid = grid; // сетка игрового поля, двухмерный массив строк
    this.actors = actorsArray; // массив движущихся объектов
    this.player = this.actors.find(actor => actor.type === 'player'); // движущийся объект, тип которого — свойство type — равно player
    this.height = grid.length; // высота игрового поля
    this.width = grid.reduce((max, elem) => (elem.length > max) ? elem.length : max, 0); // // ширина игрового поля, равна количеству ячеек в строке 
    this.status = null; // статус прохождения уровня
    this.finishDelay = 1; // таймаут после окончания игры 
  }

  isFinished() { // Определяет, завершен ли уровень
    if ((this.status !== null) && (this.finishDelay < 0)) {
      return true;
    } 
    return false;
  }

  actorAt(actor) { //Определяет, расположен ли какой-то другой движущийся объект в переданной позиции, и если да, вернёт этот объект
    if (!(actor instanceof Actor)) {
      throw new Error(`В качестве аргумента передан не объект класса Actor`);
    } 
    return this.actors.find(actorElem => actorElem.isIntersect(actor));
  }

  obstacleAt(newPosition, actorSize) { // определяет, нет ли препятствия в указанном месте
    if (!(newPosition instanceof Vector) || !(actorSize instanceof Vector)) {
      throw new Error(`В качестве аргумента передан не объект класса Vector`);
    }

    // Определяем границы на сетке, в которые перемещается объект
    const leftBorder = Math.floor(newPosition.x);
    const rightBorder = Math.ceil(newPosition.x + actorSize.x);
    const topBorder = Math.floor(newPosition.y);
    const bottomBorder = Math.ceil(newPosition.y + actorSize.y);

    // Если описанная двумя векторами область выходит за пределы игрового поля, то метод вернет строку lava,
    // если область выступает снизу. И вернет wall в остальных случаях. 
    if (bottomBorder > this.height) {
      return 'lava';
    }
    if (leftBorder < 0 || rightBorder > this.width || topBorder < 0) {
      return 'wall';
    }
    // Проходим поочередно все ячейки сетки, в диапазоне границ, найденных выше и проверяем наличие там препятствия, если оно есть - возвращаем его
    for (let y = topBorder; y < bottomBorder; y++) {
      for (let x = leftBorder; x < rightBorder; x++) {
        const obstacle = this.grid[y][x];
        if (obstacle) {
          return obstacle;
        }
      }
    }    
  }

  removeActor(actor) { // удаляет переданный объект с игрового поля
    const indexActor = this.actors.indexOf(actor);
    if (indexActor !== -1) {
      this.actors.splice(indexActor, 1);
    }
  }

  noMoreActors(type) { // определяет, остались ли еще объекты переданного типа на игровом поле
    if (this.actors.find(actor => actor.type === type)) {
      return false;
    }
    return true;
  }

  playerTouched(actorType, actor = {}) { // Меняет состояние игрового поля при касании игроком каких-либо объектов или препятствий
    if (this.status !== null) {
      return;
    }
    if (actorType === 'lava' || actorType === 'fireball') {
      this.status = 'lost';
    }
    if (actorType === 'coin' && actor.type === 'coin') {
      this.removeActor(actor);
      if (this.noMoreActors(actorType)) {
        this.status = 'won';
      }
    }
  }
}

class LevelParser { // позволяет создать игровое поле Level из массива строк
  constructor(dictionary = {}) { // словарь движущихся объектов
    this.dictionary = dictionary;
  }

  actorFromSymbol(symbol) { //Возвращает конструктор объекта по его символу, используя словарь
    if (symbol) {
      return this.dictionary[symbol];
    }    
  }

  obstacleFromSymbol(symbol) { // Возвращает строку, соответствующую символу препятствия
    switch (symbol) {
      case 'x':
        return 'wall'
        break;
      case '!':
        return 'lava'
        break;
      default:
        return undefined;
        break;
    }
  }

  createGrid(plan) { // возвращает сетку, без движущихся объектов
    return plan.map(line => line.split('')).map(line => line.map(cell => this.obstacleFromSymbol(cell)));    
  }

  createActors(plan) { // возвращает массив движущихся объектов
    return plan.reduce((result, line, y) => {
      line.split('').forEach((cell, x) => {
        const constructor = this.actorFromSymbol(cell);
        if (typeof constructor === 'function') {
          const actor = new constructor(new Vector(x, y));
          if (actor instanceof Actor) {
            result.push(actor);
          } 
        }        
      })      
      return result;
    }, [])
  }

  parse(plan) { // создает и возвращает игровое поле, заполненное препятствиями и движущимися объектами, полученными на основе символов и словаря
    return new Level(this.createGrid(plan), this.createActors(plan));
  }
}

class Fireball extends Actor { // прототип для движущихся опасностей на игровом поле
  constructor(position = new Vector(0, 0), speed = new Vector(0, 0)) {
    super(position, new Vector(1, 1), speed);
  }

  get type() {
    return 'fireball'
  }

  getNextPosition(time = 1) { // вычисляет новую позицию на поле используя предыдущие координаты, время и скорость
    return new Vector(this.pos.x + this.speed.x * time, this.pos.y + this.speed.y * time);
  }

  handleObstacle() { // меняет вектор скороти на противоположный при столкновении
    this.speed = this.speed.times(-1);
  }

  act(time, level) { // Обновляет состояние движущегося объекта
    if (level.obstacleAt(this.getNextPosition(time), this.size)) {
      this.handleObstacle();
    } else {
      this.pos = this.getNextPosition(time);
    }    
  }
}

class HorizontalFireball extends Fireball { // горизонтальная шаровая молния
  constructor(position = new Vector(0, 0)) {
    super(position, new Vector(2, 0));
  }
}

class VerticalFireball extends Fireball { // вертикальная шаровая молния
  constructor(position = new Vector(0, 0)) {
    super(position, new Vector(0, 2));
  }
}

class FireRain extends Fireball { // вертикальная шаровая молния
  constructor(position = new Vector(0, 0)) {
    super(position, new Vector(0, 3));
    this.startPos = position;
  }

  handleObstacle() { 
    this.pos = this.startPos;
  }
}

class Coin extends Actor { // реализует поведение монетки на игровом поле
  constructor(position = new Vector(0, 0)) {
    super(position.plus(new Vector(0.2, 0.1)), new Vector(0.6, 0.6));
    this.springSpeed = 8;
    this.springDist = 0.07;
    this.spring = Math.random() * Math.PI * 2;
    this.startPos = this.pos;
  }

  get type() {
    return 'coin'
  }

  updateSpring(time = 1) { // Обновляет фазу подпрыгивания
    this.spring += this.springSpeed * time;
  }

  getSpringVector() { // Создает и возвращает вектор подпрыгивания
    return new Vector(0, Math.sin(this.spring) * this.springDist)
  }

  getNextPosition(time = 1) { // Обновляет текущую фазу, создает и возвращает вектор новой позиции монетки
    this.updateSpring(time);
    return this.startPos.plus(this.getSpringVector());
  }
  act(time) { // Получает новую позицию объекта и задает её как текущую
    this.pos = this.getNextPosition(time);
  }
}

class Player extends Actor { // содержит базовый функционал движущегося объекта, который представляет игрока на игровом поле
  constructor(position = new Vector(0, 0)) {
     super(position.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5), new Vector(0, 0));
  }

  get type() {
    return 'player'
  }
}

const dictionary = { // словарь парсера
  '@': Player,
  'v': FireRain,
  'o': Coin,
  '=': HorizontalFireball,
  '|': VerticalFireball
};

const parser = new LevelParser(dictionary);

loadLevels()
  .then((res) => {
    runGame(JSON.parse(res), parser, DOMDisplay)
      .then(() => alert('Вы выиграли!'))
  });








